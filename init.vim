syntax on
set re=0
function! DoRemote(arg)
	UpdateRemotePlugins
endfunction

call plug#begin('~/.vim/plugged')

" Switch to the begining and the end of a block by pressing %
Plug 'tmhedberg/matchit'
" Better syntax-highlighting for filetypes in vim
"Plug 'sheerun/vim-polyglot'
" Intellisense engine
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'iCyMind/NeoSolarized'
Plug 'lervag/vimtex'
Plug 'Vimjas/vim-python-pep8-indent'  "better indenting for python
Plug 'tell-k/vim-autopep8'
Plug 'sirver/ultisnips'

Plug 'PatrBal/vim-textidote'
call plug#end()


colorscheme NeoSolarized
let g:neosolarized_bold = 1
let g:neosolarized_underline = 1
let g:neosolarized_italic = 1
set background=light

let g:python3_host_prog = substitute(system('which python3'),'\n','','g')
let g:python_host_prog = substitute(system('which python2'),'\n','','g')

set mouse=a  " change cursor per mode
set number  " always show current line number
set ignorecase
set smartcase  " better case-sensitivity when searching
set expandtab
set tabstop=4
set shiftwidth=4
set fillchars+=vert:\  " remove chars from seperators
set softtabstop=4
set history=1000  " remember more commands and search history
set nobackup  " no backup or swap file, live dangerously
set breakindent  " preserve horizontal whitespace when wrapping
set showbreak=..
set lbr  " wrap words
set nowrap  " i turn on wrap manually when needed
set noswapfile  " swap files give annoying warning
set scrolloff=3 " keep three lines between the cursor and the edge of the screen
set termguicolors
set noshowmode  " keep command line clean
set noshowcmd
set splitright  " i prefer splitting right and below
set splitbelow
set hlsearch  " highlight search and search while typing
set incsearch
set clipboard=unnamedplus

imap ;; <Esc>
imap ,, <Esc>
let mapleader = ","
let maplocalleader = ","
cnoremap <expr> %% getcmdtype() ==# ':' ? fnameescape(expand('%:h')) . '/' : '%%'
 
" easy split movement
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

au BufNewFile,BufEnter *.tex
	\ inoremap <Leader>alig \begin{aligned}<CR><CR>\end{aligned}<Esc>ki  |
	\ inoremap <Leader>acc \left\{\begin{aligned}<CR><CR>\end{aligned}\right.<Esc>ki  |
	\ nmap <Leader>calig v?\(\$\)\|\(equation}\)<CR>c<Leader>alig<Esc>p |
	\ map \gq ?^$\\|^\s*\(\\begin\\|\\end\\|\\label\\|\\\w*section\)?1<CR>gq//-1<CR> |
	\ omap lp ?^$\\|^\s*\(\\begin\\|\\end\\|\\label\\|\\\w*section\)?1<CR>//-1<CR>.<CR>:noh<CR> |
	\ inoremap <Leader><Leader>mul \begin{multline}<CR><CR>\end{multline}<Esc>ki |
	\ nmap <Leader>teqn cseequation<CR> |
	\ nmap <Leader>tmul csemultline<CR> |
    \ set tw=85  |
    \ nmap <Leader>scomma :%s/\C\(\(,\\|\.\)\_s*\(\\end{aligned}\)\?\(\\right.\?\)\?\)\@<!\_s*\(\(\\]\)\\|\\end{equation}\)\_s*[a-z]\?/\,\0/gc<CR> |
    \ nmap <Leader>spoint :%s/\C\(\(,\\|\.\)\_s*\(\\end{aligned}\)\?\(\\right.\?\)\?\)\@<!\_s*\(\(\\]\)\\|\\end{equation}\)\_s*[a-z]\?/\.\0/gc<CR> |
    \ set spell!

autocmd BufEnter *.py set textwidth=999 |
    \ set colorcolumn=80 |
    \ nmap <Leader>pdb oimport ipdb<CR>ipdb.set_trace()<Esc> |
    \ nmap <Leader>plt oimport matplotlib.pyplot as plt<CR><Esc> |
    \ set pumheight=5

au BufEnter *.md set tw=85 |
    \ set colorcolumn=85 

autocmd BufEnter *.edp set wrap |
    \ set tw=999 

let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
map <F5> :call UltiSnips#RefreshSnippets()<CR>

au BufNewFile,BufRead *.edp,*.idp set syntax=edp


map <F3> :tabe ~/.config/nvim/init.vim<CR>
map <C-U> <C-]>


let g:vimtex_indent_on_ampersands=0

map <leader>ss :setlocal spell!<cr>
"
"" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=

map <leader>ceqn :s/\\[\(\(.\\|\n\)\{-\}\)\\]/\\begin{equation}\1\\end{equation}/g<CR>:noh<CR>
let g:vimtex_compiler_latexmk = {
    \ 'options' : [
    \   '-pdf',
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_MultipleCompileFormats='pdf,bibtex,pdf'
let g:tex_flavor = 'latex'
set wildignore=*.aux,*.pdf,*.blg,*latexmk,*.fls
let g:vimtex_complete_bib = {'simple':0, 'abbr_fmt': '', 'custom_patterns':[], 'menu_fmt':'[@key] @author_short (@year), "@title"','match_str_fmt':'[@key] @author_all (@year), "@title"','auth_len':100}
let g:vimtex_fold_enabled = 0
let g:vimtex_view_general_viewer = 'evince'
" Disable custom warnings based on regexp
let g:vimtex_quickfix_ignore_filters = [
      \ 'Underfull',  
      \ 'Overfull', 
      \ 'breakurl Warning', 
      \ 'Token not allowed',
      \]

let g:vimtex_grammar_vlty = {
    \ 'lt_directory': '~/.config/nvim/languagetool/',
    \ 'lt_command': '',
    \ 'lt_disable': 'WHITESPACE_RULE',
    \ 'lt_enable': '',
    \ 'lt_disablecategories': '',
    \ 'lt_enablecategories': '',
    \ 'server': 'lt',
    \ 'shell_options': '',
    \ 'show_suggestions': 0,
    \ 'encoding': 'auto',
    \}

map <leader>ls <Esc>:VimtexCompileSS<CR>


let g:languagetool_jar = "~/.config/nvim/languagetool/languagetool-commandline.jar"
let g:textidote_jar = '~/.config/nvim/textidote/textidote.jar'
let g:languagetool_disable_rules='WHITESPACE_RULE,EN_QUOTES,'
    \ . 'COMMA_PARENTHESIS_WHITESPACE,CURRENCY,EN_UNPAIRED_BRACKETS,'
    \ . 'WORD_CONTAINS_UNDERSCORE,UNIT_SPACE,MULTIPLICATION_SIGN,NON_STANDARD_WORD'

nnoremap <silent> K <Esc>:call ShowDocumentation()<CR>
" Show hover when provider exists, fallback to vim's builtin behavior.
function! ShowDocumentation()
if CocAction('hasProvider', 'hover')
  call CocActionAsync('definitionHover')
else
  call feedkeys('K', 'in')
endif
endfunction

"inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#confirm() : "\<Tab>"
"inoremap <expr><CR> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
inoremap <silent><expr> <c-space> coc#refresh()
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<tab>\<CR>\<c-r>=coc#on_enter()\<CR>"
nmap <silent> <CR>F <Plug>(coc-definition)
function! s:jump_definition()
    call CocAction('jumpDefinition', 'vsplit')
endfunction
nnoremap <silent> <leader>F :call <SID>jump_definition()<CR>
    
function! s:jump_definition2()
    call CocAction('jumpDefinition')
endfunction
nnoremap <silent> <leader>U :call <SID>jump_definition2()<CR>

