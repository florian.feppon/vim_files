# Vim-Configuration files    
        
This repository contains my up-to-date vim configuration file, specifically tuned to    
edit python and Latex files. 

## Installation

For the installation, please use the following 
[https://gitlab.com/florian.feppon/software-recipes/-/blob/public-master/install_nvim.py?ref_type=heads](script).   
You will need Python3 accessible from the command line. 


## Advice for starting with (neo)vim

At this point, you can start using neovim (or vim) by running the command `nvim` or
`vim`.
If you have never used it before, you can learn the basics with the tutorial:

```bash
# For neovim
nvim -c ':Tutor'

# For vim users
vimtutor
```


